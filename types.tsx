export type RootStackParamList = {
  Root: undefined;
  NotFound: undefined;
};

export type BottomTabParamList = {
  Wallet: undefined;
  Transactions: undefined;
  Settings: undefined;
};

export type WalletParamList = {
  WalletScreen: undefined;
};

export type TransactionsParamList = {
  TransactionsScreen: undefined;
};

export type SettingsParamList = {
  SettingsScreen: undefined;
};

export enum TX_DIRECTION {
	IN = 'IN',
	OUT = 'OUT'
}

export enum PIN_STATUS {
	CHOOSE = 'choose',
	CONFIRM = 'confirm',
	ENTER = 'enter'
}