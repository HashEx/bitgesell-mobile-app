export interface ITransaction {
    status: string;
    currency: string;
    amount: number;
    to: string;
    from: string;
    fee: number;
    confirms: number;
    block: number;
    date: string;
    hash: string;
}