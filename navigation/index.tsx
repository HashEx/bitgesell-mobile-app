import { NavigationContainer, DefaultTheme, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { ColorSchemeName } from 'react-native';

import NotFoundScreen from '../screens/NotFoundScreen';
import AuthScreen from '../screens/AuthScreen';
import SeedGeneratorScreen from '../screens/SeedGeneratorScreen';
import ImportScreen from '../screens/ImportScreen';
import ConfirmSeedScreen from '../screens/ConfirmSeedScreen';
import SetPINScreen from '../screens/SetPinScreen';
import CreatingWalletSuccessScreen from '../screens/CreatingWalletSuccessScreen';
import TransactionScreen from '../screens/TransactionScreen';
import SendScreen from '../screens/SendScreen';
import ReceiveScreen from '../screens/ReceiveScreen';
import ConfirmTransactionScreen from '../screens/SendConfirmationScreen';
import EnterPINScreen from '../screens/EnterPINScreen';

import BottomTabNavigator from './BottomTabNavigator';
import LinkingConfiguration from './LinkingConfiguration';

import styles from '../styles';
import TransactionSentScreen from '../screens/TransactionSentScreen';



const navigationOptions = {
  headerStyle: styles.headerStyle,
  headerTitleStyle: styles.headerTitleStyle,
  headerTintColor: '#fff'
}

const AuthStack = createStackNavigator();

const AuthNavigator = () => {
  return (
    <AuthStack.Navigator initialRouteName="Main">
      <AuthStack.Screen name="SignIn" component={AuthScreen} />
      <AuthStack.Screen name="SeedGenerator" component={SeedGeneratorScreen} />
      <AuthStack.Screen name="ConfirmSeed" component={ConfirmSeedScreen} />
      <AuthStack.Screen name="SetPIN" component={SetPINScreen} />
      <AuthStack.Screen name="WalletSuccess" component={CreatingWalletSuccessScreen} />
      <AuthStack.Screen name="ImportSeed" component={ImportScreen} />
      <AuthStack.Screen name="Main" component={BottomTabNavigator} />
      <AuthStack.Screen name="Transaction" component={TransactionScreen} />
      <AuthStack.Screen name="Send" component={SendScreen} />
      <AuthStack.Screen name="Receive" component={ReceiveScreen} />
      <AuthStack.Screen name="ConfirmTransaction" component={ConfirmTransactionScreen} />
      <AuthStack.Screen name="ConfirmSendWithPin" component={EnterPINScreen} />
      <AuthStack.Screen name="SentTxInfo" component={TransactionSentScreen} />
      {/*<AuthStack.Screen name="QrScanner" component={QrCodeScannerScreen} />*/}
    </AuthStack.Navigator>
  )
}


export default function Navigation({ colorScheme }: { colorScheme: ColorSchemeName }) {
  return (
    <NavigationContainer
      linking={LinkingConfiguration}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}>
      <AuthNavigator />
    </NavigationContainer>
  );
}