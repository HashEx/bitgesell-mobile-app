import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";

import Transaction from "./Transaction";
import Loader from "./Loader";
import { ITransaction } from "../interfaces/Transaction";

const NoTransactionsText: React.FC = () => (
	<View>
		<Text style={styles.noTransactionsText}>
			{'transactions:no_txs'}
		</Text>
	</View>
);

interface IProps {
    transactions: ITransaction[];
    onTxPress: (tx: ITransaction) => () => void;
    loading?: boolean;
}

export const TransactionsList: React.FC<IProps> = ({ transactions, onTxPress, loading }) => {
    const renderItem = ({ item }: {item: ITransaction}) => (
        <Transaction
            tx={item}
            onPress={onTxPress(item)}
        />
    )
    const keyExtractor = (item: ITransaction, index: number) => item.hash + "" + index;
    if(loading) return <View><Loader/></View>
    return (
        <View>
            <FlatList
                data={transactions}
                renderItem={renderItem}
                keyExtractor={keyExtractor}
            />
            {(!transactions.length) && <NoTransactionsText />}
        </View>
    );
}

export default TransactionsList;

TransactionsList.defaultProps = {
	transactions: []
};

const styles = StyleSheet.create({
	noTransactionsText: {
		marginTop: 20,
		fontSize: 15,
		color: "#b6b8b5",
		alignItems: "center",
		textAlign: "center"
	},
	noTransactionsCaption: {
		fontSize: 14
	}
});
