import React from 'react';
import { View, StyleSheet } from 'react-native';

import { Label } from '../components/Input';
import { Caption } from '../components/StyledText';

const InfoItem: React.FC<{label: string}> = ({label, children}) => {
	return (
		<View style={styles.infoItem}>
			<Label style={styles.infoItemLabel}>
				{label}
			</Label>
			<Caption style={styles.caption}>{children}</Caption>
		</View>
	)
}

export default InfoItem;

const styles = StyleSheet.create({
	infoItem: {
		alignSelf: 'flex-start',
		marginBottom: 15
	},
	infoItemLabel: {
		alignSelf: 'flex-start',
		marginBottom: 9
	},
	caption: {
		alignSelf: 'flex-start',
	}
})