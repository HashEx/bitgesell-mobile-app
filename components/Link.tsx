import React from 'react';
import { View, Text, TextProps } from 'react-native';

import baseStyles from '../styles';

import Touchable from './Touchable';

interface IProps extends TextProps {
    
}

const Link: React.FC<IProps> = ({onPress, children, style}) => {
    return (
        <Touchable onPress={onPress}>
            <View>
                <Text style={[baseStyles.link, style]}>{children}</Text>
            </View>
        </Touchable>
    )
}

export default Link;
