import React from 'react';
import { View, ActivityIndicator } from 'react-native';

import Colors from '../constants/Colors';

interface IProps {
    size?: "large" | "small";
    color?: string;
}

const Loader: React.FC<IProps> = ({ size = "large", color = Colors.primary }) => {
    return (
        <View>
            <ActivityIndicator
                size={size}
                color={color}
            />
        </View>
    )
}

export default Loader;