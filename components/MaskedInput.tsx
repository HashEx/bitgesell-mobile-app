import React, { useState, useRef } from 'react';
import { TextInput } from 'react-native';

interface IProps {
    value: string;
    currencySymbol?: string;
    maskType?: string;
    onChangeText?: (value: string) => void;
}

const MaskedInput: React.FC<IProps> = ({ currencySymbol, maskType, onChangeText, ...props }) => {

    const [value, setValue] = useState<string>(props.value || "")
    const refInput = useRef(null)


    const _onChangeText = (value) => {
        value = value.split(currencySymbol).join("");

        if (value != "") {
            if (maskType == "money") {
                value = value.replace(/\D/g, "");
                onChangeText(value);
                value = value + ' ' + currencySymbol
            }

        }

        setValue(value);
    }

    const _onSelection = ({ nativeEvent: { selection } }) => {
        if (!currencySymbol) {
            return;
        }

        const length = currencySymbol.length;
        const maxSelectionPosition = value.length - length - 1;

        const start = selection.start > maxSelectionPosition ? maxSelectionPosition : selection.start;
        const end = selection.end > maxSelectionPosition ? maxSelectionPosition : selection.end;

        refInput.current.setNativeProps({
            selection: {
                start: start,
                end: end
            }
        })
    }
    return (
        <TextInput
            ref={refInput}
            {...props}
            onSelectionChange={_onSelection}
            value={value}
            onChangeText={_onChangeText}
        />
    );
}

export default MaskedInput;