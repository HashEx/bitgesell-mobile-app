import * as React from 'react';
import { StyleSheet } from 'react-native';
import { Text, TextProps } from './Themed';

export const MonoText: React.FC<TextProps> = (props) => {
  return <Text {...props} style={[props.style, { fontFamily: 'space-mono' }]} />;
}

export const Header1Text: React.FC<TextProps> = ({style, ...props}) => (
	<Text {...props} style={[styles.h1, style]} />
);

export const Header2Text: React.FC<TextProps> = ({style, ...props}) => (
	<Text {...props} style={[styles.h2, style]} />
);

export const Header3Text: React.FC<TextProps> = ({style, ...props}) => (
	<Text {...props} style={[styles.h3, style]} />
)

export const Caption: React.FC<TextProps> = ({style, ...props}) => (
	<Text {...props} style={[styles.caption, style]} />
);

export const Bold: React.FC<TextProps> = ({style, ...props}) => (
	<Text {...props} style={[styles.bold, style]} />
);

const styles = StyleSheet.create({
	h1: {
		color: '#fff',
		fontSize: 25,
		fontWeight: 'bold',
		lineHeight: 35.5,
		marginBottom: 23,
		alignSelf: 'flex-start'
	},
	h2: {
		color: '#fff',
		fontSize: 18,
		marginBottom: 10,
		alignSelf: 'flex-start'
	},
	h3: {
		color: '#fff',
		fontSize: 16,
		fontWeight: 'bold',
		lineHeight: 24,
		marginBottom: 17,
		alignSelf: 'flex-start',
	},
	caption: {
		color: 'rgba(255,255,255,0.54)',
		fontSize: 16,
		lineHeight: 24,
		alignSelf: 'flex-start',
	},
	bold: {
		fontWeight: 'bold',
		color: '#fff',
	}
});