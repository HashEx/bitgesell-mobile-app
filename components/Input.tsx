import React from 'react';
import { Text, View, TextInput, StyleSheet } from 'react-native';
import { Feather } from '@expo/vector-icons';
import { Row } from 'react-native-easy-grid';

import Colors from '../constants/Colors';
import MaskedInput from './MaskedInput';


export const Label = ({children, style}) => {
	return (
		<Text style={[styles.label, style]}>{children}</Text>
	)
}

export default class Input extends React.Component {
	render(){
		const { style, ...props} = this.props;
		return (
			<TextInput
				style={[styles.input, style]}
				underlineColorAndroid="rgba(0,0,0,0)"
				placeholderTextColor="rgb(166,166,166)"
				{...props}
			/>
		)
	}
}

const ValidIcon = () => (
	<Feather
		style={[styles.icon, styles.iconValid]}
		size={24}
		name="check"
	/>
);

const InvalidIcon = () => (
	<Feather
		style={[styles.icon, styles.iconInvalid]}
		size={24}
		name="x"
	/>
);

export const InputGroup = ({style, children, label, suffix, isValid, isInvalid, caption, captionStyle, inputStyle, labelStyle, ...props}) => {
	const wrapperStyles = [styles.inputWrapper];
	let icon = null;
	if(isValid && !isInvalid){
		icon = <ValidIcon />;
		wrapperStyles.push(styles.inputWrapperValid);
	}
	if(!isValid && isInvalid){
		icon = <InvalidIcon />;
		wrapperStyles.push(styles.inputWrapperInvalid);
	}
	return (
		<View style={style}>
            {label && <View style={labelStyle}>{label}</View>}
            <View style={wrapperStyles}>
				{/* <View style={{flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}> */}
					<Input {...props} style={[styles.inputInWrapper, inputStyle]} />
					{suffix ? <Text style={styles.suffix}>{suffix}</Text>
						: null }
				{/* </View> */}
                {/* {icon} */}
            </View>
            <View style={[styles.caption, captionStyle]}>
            	{caption}	
            </View>
        </View>
	)
}

const styles = StyleSheet.create({
	label: {
		fontWeight: 'bold',
		color: "#fff",
		fontSize: 16,
		alignSelf: 'flex-start'
	},
	input: {
		paddingTop: 13,
		paddingBottom: 13,
		paddingLeft: 17,
		paddingRight: 5,
        color: "#fff",
        backgroundColor: "#36373C",
		borderRadius: 5,
		flex: 1,
	},
	caption: {
        paddingTop: 5,
        paddingBottom: 5,
        alignItems: "flex-end",
	},
	inputWrapper: {
		borderColor: "rgb(166,166,166)",
        borderRadius: 5,
		borderWidth: 1,
        backgroundColor: "#36373C",
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignSelf: 'stretch',
		overflow: 'hidden'
	},
	suffix: {
        color: "#fff",
		// backgroundColor: "#36373C",
		alignSelf: 'center',
		marginEnd: 17
	},
	inputInWrapper: {
		paddingEnd: 10 
	},
	inputWrapperValid: {
		borderColor: Colors.valid,
	},
	inputWrapperInvalid: {
		borderColor: Colors.invalid
	},
	icon: {
		position: 'absolute',
		right: 5,
		top: 8
	},
	iconValid: {
		color: Colors.valid
	},
	iconInvalid: {
		color: Colors.invalid	
	}
})