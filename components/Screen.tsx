import React from 'react';
import { ScrollView, StyleSheet, ScrollViewProps } from 'react-native';

import baseStyles from '../styles';

const Screen: React.FC<ScrollViewProps> = ({ style, children, ...props }) => {
    return (
        <ScrollView style={[baseStyles.screen, style]} {...props}>
            {children}
        </ScrollView>
    )
};

export default Screen;