import React, { useState } from 'react';

import { Alert, View, Text, StyleSheet } from 'react-native';

import shuffleArray from '../utils/shuffleArray';

import Touchable from './Touchable';
import { PrimaryButton } from './StyledButton';

interface ISeedWordButton {
    onPress: () => void;
    disabled?: boolean;
}

const SeedWordButton: React.FC<ISeedWordButton> = ({children,onPress, disabled}) => {
    const buttonStyle: any = [styles.wordButton];
    const buttonTextStyle: any = [styles.wordButtonText];
    if(disabled){
        buttonStyle.push(styles.wordButtonDisabled);
        buttonTextStyle.push(styles.wordButtonTextDisabled);
    }
    return (
        <Touchable onPress={onPress} disabled={disabled}>
            <View style={buttonStyle}>
                <Text style={buttonTextStyle}>{children}</Text>
            </View>
        </Touchable>
    )
}

interface ISeedContstructorProps {
    seed: string;
    onContinue: () => void;
}

export const SeedConstructor: React.FC<ISeedContstructorProps> = ({seed, onContinue}) => {
    const [words, setWords] = useState<string[]>([]);
    // TODO: Uncomment after debug finish
    // const [seedWords] = useState<string[]>(shuffleArray(seed.split(" ")));
    const [seedWords] = useState<string[]>(seed.split(" "));
	const _onWordPress = (word: string) => () => {
		setWords(prevState => ([...prevState, word]));
	}
	const _onRemoveWord = (word: string) => () => {
		setWords(prevState => {
            const index = prevState.indexOf(word);
			return [
				...prevState.slice(0, index),
				...prevState.slice(index + 1)
			]
		})
	}
	const _handleContinuePress = () => {
		if(words.join(" ") === seed){
			onContinue();
		}else{
			Alert.alert('', 'confirm_seed:errors.invalid_seed');
			setWords([]);
		}
	}
    const buttonDisabled = seedWords.length !== words.length; 
    return (
        <View>
            <View style={styles.words}>
                {words.map((word, index) => {
                    const onPress = _onRemoveWord(word);
                    return (
                        <SeedWordButton
                            key={index}
                            onPress={onPress}
                        >
                            {word}
                        </SeedWordButton>
                    )
                })}
            </View>
            <View style={styles.wordsButtons}>
                {seedWords.map((word, index) => {
                    const disabled = words.indexOf(word) > -1;
                    return (
                        <SeedWordButton
                            key={index}
                            onPress={_onWordPress(word)}
                            disabled={disabled}
                        >
                            {word}
                        </SeedWordButton>
                    )
                })}
            </View>
            <View>
                <PrimaryButton
                    disabled={buttonDisabled}
                    onPress={_handleContinuePress}
                >
                    common:continue
                </PrimaryButton>
            </View>
        </View>
    );
}

export default SeedConstructor;

const styles = StyleSheet.create({
	words: {
		height: 200,
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'center',
		justifyContent: 'center',
	},
	word: {

	},
	wordsButtons: {
		flexDirection: 'row',
		flexWrap: 'wrap',
		alignItems: 'center',
		justifyContent: 'center',
		marginTop: 10,
		marginBottom: 30
	},
	wordButton: {
		padding: 5,
		borderRadius: 5,
		margin: 5,
		backgroundColor: 'rgba(255, 255, 255, 0.14)'
	},
	wordButtonDisabled: {
		backgroundColor: 'rgba(255, 255, 255, 0.05)'	
	},
	wordButtonText: {
		color: '#fff',
		fontSize: 14
	},
	wordButtonTextDisabled: {
		color: 'rgba(255, 255, 255, 0)'
	}
});