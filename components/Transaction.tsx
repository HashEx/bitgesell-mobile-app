import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { Feather } from '@expo/vector-icons';

import { Caption } from '../components/StyledText';
import Touchable from '../components/Touchable';

import Colors from '../constants/Colors';
import { ITransaction } from '../interfaces/Transaction';
import { TX_DIRECTION } from '../types';

const TransactionIcon = ({direction}: {direction: TX_DIRECTION}) => {
	if(direction === TX_DIRECTION.IN){
		return (
			<View style={[styles.icon, styles.iconOut]}>
				<Feather
					name="arrow-right"
					color={Colors.primary}
					size={24}
				/>
			</View>
		)
	}else{
		return (
			<View style={[styles.icon, styles.iconIn]}>
				<Feather
					name="arrow-left"
					color="#fff"
					size={24}
				/>
			</View>
		)
	}
}

interface ITransactionProps {
    tx: ITransaction,
    onPress: () => void;
}

export const Transaction: React.FC<ITransactionProps> = ({tx, onPress}) => {
    const direction = tx.amount > 0 ? TX_DIRECTION.IN : TX_DIRECTION.OUT;
    return (
        <Touchable onPress={onPress}>
            <View style={styles.container}>
                <View style={styles.iconWrapper}>
                    <TransactionIcon direction={direction} />
                </View>
                <View style={styles.textWrapper}>
                    <View style={styles.txInfo}>
                        <Text numberOfLines={1} style={styles.info}>{'transaction:status.' + tx.status}</Text>
                    </View>
                    <View style={styles.txInfo}>
                        <Text numberOfLines={1} style={styles.info}>{tx.amount} {tx.currency}</Text>
                    </View>
                    <View style={styles.txInfo}>
                        <Caption numberOfLines={1} style={styles.caption}>{'transaction:sent_to'}: {tx.to}</Caption>
                    </View>
                    <View style={styles.txInfo}>
                        <Caption numberOfLines={1} style={styles.caption}>{tx.date}</Caption>
                    </View>
                </View>
            </View>
        </Touchable>
    )
}

export default Transaction;

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		marginTop: 10,
		marginBottom: 25
	},
	icon: {
		padding: 10,
		borderRadius: 25
	},
	iconIn: {
		backgroundColor: 'rgba(149, 154, 166, 0.27)',
	},
	iconOut: {
		backgroundColor: 'rgba(179, 125, 248, 0.2)',
	},
	textWrapper: {
		flex: 1
	},
	iconWrapper: {
		marginRight: 15
	},
	txInfo: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	info: {
		color: '#fff',
		fontSize: 15
	},
	caption: {
		marginTop: 0,
		fontSize: 14
	}
});