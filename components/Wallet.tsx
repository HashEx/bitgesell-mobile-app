import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

import Loader from '../components/Loader';

const formatValue = (value: number) => {
	const valueString = value.toString();
	if(valueString.length <= 15){
		return value
	}
	return valueString.slice(0, 15) + '...';
}

interface IProps {
    title: string;
    value: number;
    loading: boolean;
}

const Wallet: React.FC<IProps> = ({ title, value, loading }) => {
    return (
        <View style={styles.container}>
            <View style={styles.walletTitle}>
                <Text style={styles.textCoin}>{title}</Text>
            </View>
            {loading ? <Loader size="small" /> : (
                <Text style={styles.textBalance} numberOfLines={1}>
                    {value}
                </Text>
            )}
        </View>
    )
};

export default Wallet;

const styles: any = StyleSheet.create({
	container: {
		borderRadius: 10,
		backgroundColor: '#3b3c41',
		padding: 30
	},
	walletTitle: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	textCoin: {
		fontSize: 16,
		color: '#fff',
		marginBottom: 10,
		fontWeight: 'bold'
	},
	textExchange: {
		fontSize: 16,
		color: '#fff',
		color: 'rgba(255,255,255,0.54)'
	},
	textBalance: {
		color: '#fff',
		fontSize: 24
	}
})