import React from "react";
import { View, Text, TouchableOpacity, } from "react-native";

import Touchable from './Touchable';
import Loader from './Loader';

import Colors from '../constants/Colors';

interface IProps {
    accessibilityLabel?: string;
    onPress?: () => void;
    hasTVPreferredFocus?: boolean;
    disabled?: boolean;
    testID?: string;
    loading?: boolean;
}

interface IStyledButtonProps extends IProps {
    type: "primary" | "secondary";
}


const StyledButton: React.FC<IStyledButtonProps> = (props) => {
    const {
        type = "primary",
        accessibilityLabel,
        onPress,
        children,
        hasTVPreferredFocus,
        disabled,
        testID,
        loading
    } = props;
    const buttonStyles = [styles.base.button];
    const textStyles = [styles.base.text];
    if (type) {
        buttonStyles.push(styles[type].button);
        textStyles.push(styles[type].text);
    }
    const accessibilityStates = [];
    if (disabled) {
        buttonStyles.push(styles.base.buttonDisabled);
        textStyles.push(styles.base.textDisabled);
        if (type) {
            buttonStyles.push(styles[type].buttonDisabled);
            textStyles.push(styles[type].textDisabled);
        }
        accessibilityStates.push("disabled");
    }
    return (
        <Touchable
            accessibilityLabel={accessibilityLabel}
            accessibilityRole="button"
            accessibilityStates={accessibilityStates}
            hasTVPreferredFocus={hasTVPreferredFocus}
            testID={testID}
            disabled={disabled}
            onPress={onPress}
        >
            <View style={buttonStyles}>
                {loading ? (
                    <Loader size="small" color={styles[type].text.color} /> 
                ) : (
                    <Text style={textStyles}>
                        {children}
                    </Text>
                )}
            </View>
        </Touchable>
    );
}

export const PrimaryButton: React.FC<IProps> = props => {
	return <StyledButton type="primary" {...props} />;
};

export const SecondaryButton: React.FC<IProps> = props => {
	return <StyledButton type="secondary" {...props} />;
};

export const BorderlessButton: React.FC<IProps> = props => {
	return (
        <TouchableOpacity onPress={props.onPress}>
            <Text style={styles.borderless} {...props} />
        </TouchableOpacity>
    )
}

const styles: any = {
	base: {
		button: {
			borderRadius: 25,
			alignItems: "center",
			padding: 15,
			marginBottom: 10
		},
		text: {
			color: "#fff",
			fontSize: 16,
			fontWeight: "bold"
		},
		buttonDisabled: {},
		textDisabled: {}
	},
	borderless: {
		color: '#fff',
		fontWeight: 'bold',
		fontSize: 18,
		padding: 12
	},
	primary: {
		button: {
			backgroundColor: Colors.primary
		},
		text: {
			color: "#fff"
		},
		buttonDisabled: {
			backgroundColor: "rgba(164, 125, 248, 0.52)"
		},
		textDisabled: {}
	},
	secondary: {
		button: {
			backgroundColor: "#ffffff"
		},
		text: {
			color: "#000"
		},
		buttonDisabled: {},
		textDisabled: {}
	}
};
