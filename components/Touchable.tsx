import React from 'react';
import { Platform, TouchableNativeFeedback, TouchableOpacity } from 'react-native';

const Touchable: React.FC<any> = (props) => {
    return Platform.OS === 'android' ? <TouchableNativeFeedback {...props} /> : <TouchableOpacity {...props} />;
}

export default Touchable