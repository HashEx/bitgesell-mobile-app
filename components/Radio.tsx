import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';

import Touchable from './Touchable';

var styles = StyleSheet.create({
  outerCircle: {
    height: 20,
    width: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    backgroundColor: 'rgba(164, 125, 248, 0.32)'
  },
  innerCircle: {
    height: 16,
    width: 16,
    borderRadius: 8
  },
  circle: {
    padding: 10
  },
  option: {
    flexDirection: 'row',
    alignItems: 'center'
  }
});

interface ICirlceProps {
    color?: string,
    selectedColor?: string;
    isSelected?: boolean;
}

const Circle = ({ color, isSelected, selectedColor }: ICirlceProps) => {

    let innerCircle;
    let appliedColor;

    if (isSelected) {
      appliedColor = selectedColor;
      innerCircle = <View style={[styles.innerCircle, { backgroundColor: appliedColor }]}/>;
    } else {
      appliedColor = color;
      innerCircle = null;
    }

    return (
      <View style={styles.circle}>
        <View style={[styles.outerCircle, { borderColor: appliedColor }]}>
          {innerCircle}
        </View>
      </View>
    );
}

interface IOptionProps {
    onPress?: () => void;
    isSelected?: boolean;
    color?: string;
    selectedColor?: string;
}

export const Option: React.FC<IOptionProps> = ({ onPress, isSelected, color, selectedColor, children }) => {
    return (
      <Touchable onPress={onPress}>
        <View style={styles.option}>
          <Circle color={color} selectedColor={selectedColor} isSelected={isSelected}/>
          <View style={{ flex: 1 }}>
            {children}
          </View>
        </View>
      </Touchable>
    );
};

interface IRadioProps {
    defaultSelect: number;
    onSelect: (index: number) => void;
    style: any;
}

const Radio: React.FC<IRadioProps> = ({onSelect, defaultSelect = -1, style, children}) => {
    const [selectedIndex, setSelectedIndex] = useState(-1);
    const _onSelect = (index: number) => {
        setSelectedIndex(index);
        onSelect(index);
    };
    const targetIndex = selectedIndex !== -1? selectedIndex : defaultSelect;

    const _children = React.Children.map(children, (child: any, index: number) => {
      if (child.type === Option) {
        const value = index + 1;
        return React.cloneElement(child, {
          onPress: () => _onSelect(value),
          isSelected: value == targetIndex
        });
      }

      return child;
    });

    return (
      <View style={style}>
        {_children}
      </View>
    );
}

export default Radio;
