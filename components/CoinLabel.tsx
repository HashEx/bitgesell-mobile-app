import React from 'react';
import { Text, View, StyleSheet, TextProps } from 'react-native';


const CoinLabel: React.FC<TextProps> = ({ children, style }) => {
    return (
        <View style={[styles.container, style]}>
            <Text style={styles.text}>
                {children || "BGL"}
            </Text>
        </View>
    )
}

export default CoinLabel;

const styles = StyleSheet.create({
	container: {
		margin: 5,
		borderRadius: 3,
		borderWidth: 0.8,
		borderColor: 'rgba(255, 255, 255, .57)'
	},
	text: {
		padding: 5,
		fontSize: 16,
		letterSpacing: 1.3,
		color: 'rgba(255, 255, 255, .57)',
		textAlign: 'center',
		fontWeight: 'bold'
	}
})