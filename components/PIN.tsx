import React from 'react';
import { Alert, ScrollView, View, Text, StyleSheet, AsyncStorage, Vibration } from 'react-native';
import { Row, Col, Grid } from 'react-native-easy-grid';
import { SimpleLineIcons, Feather } from '@expo/vector-icons';

import Touchable from './Touchable';
import { Header1Text, Caption } from './StyledText';
import { PIN_STATUS } from '../types';


const Circle: React.FC<{ isActive: boolean}> = ({isActive}) => {
	const style = [styles.pinCircle];
	if(isActive){
		style.push(styles.pinCircleActive);
	}
	return  <View style={style} />;
}

const Code: React.FC<{value: number[], pinLength: number}> = ({value, pinLength}) => {
	const circles = [];
	for(let i = 0; i < pinLength; i++){
		circles.push(<Circle key={i} isActive={!!value[i]} />)
	}
	return (
		<View style={styles.code}>
			{circles}
		</View>
	)
}

const NumberButton: React.FC<{onPress: () => void}> = ({children, onPress}) => {
	return (
		<Touchable onPress={onPress}>
			<Text style={styles.numberButton}>
				{children}
			</Text>	
		</Touchable>
		
	)
}

const KEYBOARD = [['1','2','3'], ['4','5','6'], ['7','8','9']];

const Keys = ({keys, onPress}) => {
	return keys.map((key, index) => (
		<Col key={index}>
			<NumberButton onPress={onPress(key)}>
				{key}
			</NumberButton>
		</Col>
	))
}

const Keyboard = ({onPress, onDelete}) => {
	return (
		<View style={styles.keyboard}>
			<Grid>
				{KEYBOARD.map((row, index) => (
					<Row key={index}>
						<Grid>
							<Keys
								keys={row}
								onPress={onPress}
							/>
						</Grid>
					</Row>
				))}
				<Row>
					<Grid>
						<Col />
						<Col>
							<NumberButton onPress={onPress('0')}>
								0
							</NumberButton>
						</Col>
						<Col>
							<NumberButton onPress={onDelete}>
								<Feather size={24} name="delete" />
							</NumberButton>
						</Col>
					</Grid>
				</Row>
			</Grid>
		</View>
	)
}

interface IProps {
    status: PIN_STATUS
    onConfirm?: (code: string) => void;
    onEnter: (code: string) => void;
}

interface IState {
    enteredCode: string
    status: PIN_STATUS
    code: number[]
    pinLength: number
    retries: 3,
    error: boolean
}

export class PIN extends React.Component<IProps, IState> {

	constructor(props: IProps){
		super(props);
		this.state = {
			enteredCode: "",
			code: [],
			status: props.status || PIN_STATUS.CHOOSE,
			pinLength: 6,
			retries: 3,
			error: false
		}
	}
	_getPinAsync = async () => {
		return await AsyncStorage.getItem('pin');
	}
	_savePinAsync = async (pin: string) => {
		return await AsyncStorage.setItem('pin', pin);
	}

	_lastCode = '';

	_handleKeyPress = (number: number) => async () => {
		let newState: any = {}
		const { code, pinLength, status, enteredCode } = this.state;

		if(code && this._lastCode !== code.join('')) {
			console.log('state is not updated yet', code.join(''), this._lastCode);
			return;
		}

		const newCode = [...code, number];
		this._lastCode = newCode.join('');

		if(newCode.length === pinLength){
			switch(status){
				case PIN_STATUS.CHOOSE: {
					newState = {
						status: PIN_STATUS.CONFIRM,
						code: [],
						enteredCode: newCode.join("")
					}
					break;
				}
				case PIN_STATUS.CONFIRM: {
					if(newCode.join("") === enteredCode){
						this._savePinAsync(enteredCode).then(() => {
							if(this.props.onConfirm) {
								this.props.onConfirm(enteredCode);
							}
						});
					}else{
						Alert.alert("Error", "Entered passcode do not match");
						newState = {
							status: PIN_STATUS.CHOOSE,
							code: [],
							enteredCode: null,
							error: true
						}
					}
					break;
				}
				case PIN_STATUS.ENTER: {
					const pin = await this._getPinAsync();
					if (pin === newCode.join("")) {
						this.props.onEnter(pin);
					} else {
						Alert.alert("Error", "Entered passcode is not correct");
						this.setState({ code: [] });
					}
					break;
				}
				default: return;
			}
		} else{
			newState.code = newCode;
		}
		this._lastCode = newState.code ? newState.code.join('') : '';
		this.setState(newState as IState);
		Vibration.vibrate(30);
	}
	_handleDeletePress = () => {
		this.setState(prevState => {
			const newCode = prevState.code.slice(0, -1);
			this._lastCode = newCode.join('');
			return {
				code: newCode
			}
		});	
		Vibration.vibrate(30);
	}
	render(){
		const { code, status, pinLength } = this.state;
		return (
			<View style={{flex: 1, justifyContent:'center'}}>
				<View style={styles.texts}>
					<SimpleLineIcons
						name="lock"
						size={36}
						style={styles.lockIcon}
					/>
				</View>
				<View style={styles.texts}>
					<Header1Text style={styles.centeredText}>{'pin:titles.' + status}</Header1Text>
					<Caption style={styles.centeredText}>{'pin:description'}</Caption>
				</View>
				<View style={styles.texts}>
					<Code value={code} pinLength={pinLength} />
				</View>
				<View>
					<Keyboard
						onPress={this._handleKeyPress}
						onDelete={this._handleDeletePress}
					/>
				</View>
			</View>
		)
	}
}

export default PIN;

const styles = StyleSheet.create({
	lockIcon: {
		color: '#fff',
		marginBottom: 10
	},
	texts: {
		alignItems:'center',
		alignSelf: 'center',
	},
	centeredText: {
		textAlign: 'center',
		alignSelf: 'center'
	},
	pinCircle: {
		borderRadius: 5,
		margin: 1,
		width: 10,
		height: 10,
		backgroundColor: 'rgba(255,255,255,0.5)'
	},
	pinCircleActive: {
		borderRadius: 6,
		margin: 0,
		width: 12,
		height: 12,
		backgroundColor: 'rgba(255,255,255,1)'
	},
	code: {
		width: '70%',
		paddingTop: 20,
		paddingBottom: 20,
		paddingLeft: 30,
		paddingRight: 30,
		borderBottomWidth: 0.5,
		borderBottomColor: 'rgba(255,255,255,0.5)',
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-around',
		marginTop: 30,
		marginBottom: 30
	},
	keyboard: {
		height: 260,
		width: '100%'
	},
	numberButton: {
		padding: 10,
		fontSize: 28,
		color: '#fff',
		textAlign: 'center'
	}
})
