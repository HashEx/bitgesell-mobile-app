import Colors from '../constants/Colors';

export default {
	headerStyle: {
		backgroundColor: Colors.headerBackground,
		borderBottomColor: Colors.headerBackground
	},
	headerTitleStyle: {
		color: Colors.headerText
	},
	screen: {
		backgroundColor: Colors.background
	},
	link: {
		color: Colors.primary,
		textDecorationLine: 'underline',
		alignSelf: 'flex-start'
	},
	textCenter: {
		textAlign: 'center'
	},
	contentCenter: {
		alignItems: 'center'
	}
}