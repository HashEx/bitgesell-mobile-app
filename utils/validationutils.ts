export const isNumber = (value: any) => {
    return !isNaN(value);
}

export const isStrBlank = (str: string) => {
    return str.trim() == '';
}

export const onlyLetters = (str: string) => {
    const regex = new RegExp(/^[A-Za-z]+$/);
    return regex.test(str);
}