export const _1Second = 1000;
export const _1Minute = 60 * _1Second;
export const _1Hour = 60 * _1Minute;
export const _1Day = 24 * _1Hour;

export const getDaysInDiff = (diff: number) => diff / _1Day;
export const getHoursInDiff = (diff: number) => diff / _1Hour;
export const getMinutesInDiff = (diff: number) => {
	return (diff - Math.floor(getHoursInDiff(diff)) * _1Hour) / _1Minute;
}

export const convertTimeStampToDateStr = (timeStamp: string)  => {
    if(!parseInt(timeStamp)){
        return null;
    }
    const date = new Date(parseInt(timeStamp) * 1000)
    return formatDate(date);
}

export const formatDate = (d: Date) => {
    const datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" +
    d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
    return datestring;
}

export const daysFrom = (timestamp: number) => {
	if(!timestamp) {
		return 0;
	}
	return Math.max(0, Math.floor(getDaysInDiff(new Date().getTime() - timestamp)));
}
