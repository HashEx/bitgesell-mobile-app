import axios from 'axios';

import { ITransaction } from "../interfaces/Transaction"

const api = axios.create({
    baseURL: 'https://bgl.hashex.org'
});

const WALLET = 'bgl1q5msz0nez928nfavg339d2syhj062n7006k954l';

// {
//     id: page * 10 + index + 1,
//     status: '',
//     currency: 'BGL',
//     amount: Math.round(5 * Math.random()) * Math.random() > 0.5 ? -1 : 1,
//     to: '',
//     from: '',
//     fee: 0.1,
//     confirms: Math.round(10 * Math.random()),
//     hash: 'hhs' + Math.round(5 * Math.random()),
//     date: '01/01/2020 13:50',
//     block: Math.round(20 * Math.random())
// } as ITransaction;

export const getTransactions = async (page:number = 1, limit: number = 10): Promise<ITransaction[]> => {
    if(page > 1) return Promise.resolve([]);
    const response = await api.get(`/listtransactions/${WALLET}`, {params: {page, limit}});
    return response.data;
    
};

export const getBalance = async (): Promise<number> => {
    const response = await api.get(`/getbalance/${WALLET}`);
    return response.data;
};

export const sendTransaction = (): Promise<any> => {
    return Promise.resolve(true)
}

export const getMyAddress = () => WALLET;