import React from 'react';
import { Text, View, StyleSheet, Linking } from 'react-native';
import { Feather } from '@expo/vector-icons';

import Link from '../components/Link';
import Screen from '../components/Screen';
import { Header1Text, Header2Text, Caption } from '../components/StyledText';
import { PrimaryButton } from '../components/StyledButton';

interface IProps {
    navigation: any;
    route: any;
}


export class TransactionSentScreen extends React.Component<IProps> {

    _handleDonePress = () => {
        const params = this.props.route.params || {};
        if (params.onTxSent) {
            params.onTxSent();
        }
        this.props.navigation.navigate(params.navigateTo, { update: true });
    }

    render() {
        const params = this.props.route.params || {};
        return (
            <Screen style={styles.container}>
                <View style={styles.mainContent}>
                    <View style={styles.center}>
                        <View style={[styles.iconWrapper, styles.center]}>
                            <Feather name="check" size={24} />
                        </View>
                    </View>
                    <Header1Text style={[styles.textCenter, { paddingTop: 10, paddingBottom: 0 }]}>
                        {'tx_sent:description'}
                    </Header1Text>
                </View>
                {params.amount ?
                    <View style={[styles.content, { paddingTop: 0 }]}>
                        <View>
                            <Text style={[styles.textCenter, styles.amount, { paddingTop: 0 }]}>{params.amount}</Text>
                            <Text style={[styles.textCenter, styles.coinLabel]}>{params.currency || 'BGL'}</Text>
                        </View>
                    </View> : null}
                {params.address ?
                    <View style={styles.content}>
                        <Header2Text style={styles.textCenter}>
                            {'tx_sent:sent_to'}
                        </Header2Text>
                        <Caption style={styles.textCenter}>
                            {params.address}
                        </Caption>
                    </View> : null}
                {params.hash ? (
                    <View style={styles.content}>
                        <Header2Text style={styles.textCenter}>
                            {'transaction:tx_hash'}
                        </Header2Text>
                        <Caption style={styles.textCenter}>
                            {params.hash}
                        </Caption>
                    </View>
                ) : null}
                <View style={styles.content}>
                    <PrimaryButton onPress={this._handleDonePress}>{'common:done'}</PrimaryButton>
                </View>
            </Screen>
        )
    }
}

export default TransactionSentScreen;

const styles = StyleSheet.create({
    container: {
        padding: 30,
    },
    mainContent: {
        paddingTop: 30,
        paddingBottom: 0
    },
    content: {
        paddingVertical: 5,
    },
    center: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    textCenter: {
        textAlign: 'center',
        alignSelf: 'center'
    },
    iconWrapper: {
        width: 46,
        height: 46,
        backgroundColor: 'rgba(59, 255, 131, 0.75)',
        borderRadius: 23,
        justifyContent: 'center',
    },
    amount: {
        fontSize: 70,
        color: '#fff'
    },
    coinLabel: {
        fontSize: 13,
        letterSpacing: 1.3,
        color: 'rgba(255, 255, 255, .57)',
        textAlign: 'center'
    }
});