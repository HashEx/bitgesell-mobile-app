import React from 'react';
import { ScrollView, View, Image, Text, StyleSheet } from 'react-native';

//import { storeKey } from '../utils/keystorage';
//import { subscribeForNotifications } from '../utils/notifications';


interface IProps {
    navigation: any;
    route: any;
}

export class CreatingWalletSuccessScreen extends React.Component<IProps> {
	componentDidMount(){
		this._saveKey();
	}
	_saveKey = async () => {
		const { pin, seed } = this.props.route.params;
		setTimeout( async () => {
			// TODO: use after crypto integration 
			//const account = await storeKey(pin, seed);
			const account = '1231';
			//subscribeForNotifications();
			this.props.navigation.navigate('Main');
		}, 1000);
	}
	render(){
		return (
			<ScrollView
				style={styles.container}
				contentContainerStyle={styles.contentContainer}
			>
				<View>
					<Text style={styles.title}>{'wallet_success:title'}</Text>
					<Text style={styles.subTitle}>{'wallet_success:description'}</Text>
				</View>
			</ScrollView>
		)
	}
}

export default CreatingWalletSuccessScreen;

const styles = StyleSheet.create({
	container: {
		
	},
	contentContainer: {
		padding: 30
	},
	title: {
		paddingTop: 30,
		fontSize: 25,
		lineHeight: 31,
		color: '#fff',
		textAlign: 'center',
		marginBottom: 15
	},
	subTitle: {
		fontSize: 16,
		lineHeight: 24,
		color: '#fff',
		textAlign: 'center',
		marginBottom: 15
	},
	image: {
		maxWidth: '100%'
	}
});