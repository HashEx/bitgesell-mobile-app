import React from "react";
import {View, Text, StyleSheet, ScrollView} from 'react-native';

import Colors from "../constants/Colors";

import baseStyles from "../styles";

import { PrimaryButton } from "../components/StyledButton";

import * as walletService from "../services/walletService";

const screenPadding = 30;

interface IProps {
    navigation: any;
    route: any;
}

interface IState {
    publishingTransaction: boolean;
}

export class SendConfimationScreen extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);
        this.state = {
            publishingTransaction: false,
        }
    }

    _handleSendTransaction = async () => {
        const { navigate } = this.props.navigation;
        navigate('ConfirmSendWithPin', { 
            onConfirmed: this._onPinConfirmed
        })
        // this._onPinConfirmed();
    }

    _onPinConfirmed = async () => {
        const params = this.props.route.params || {};

        console.log('publishing send transaction');

        this.setState({publishingTransaction: true})
        try {
            const result = await walletService.sendTransaction();
            this.setState({ publishingTransaction: false })
			this.props.navigation.navigate('SentTxInfo', {
				amount: params.amount, 
				address: params.address,
                navigateTo: 'Wallet',
                currency: 'BGL',
                hash: result.transactionHash,
			});
        } catch(ex) {
            alert('confirm_send:error.failure' + ex)
            console.log(ex)
            this.setState({ publishingTransaction: false })
        }
    }

    render() {
        const params = this.props.route.params || {};
        return (
            <ScrollView style={styles.container}>
                <Text style={{
                    fontSize: 16,
                    opacity: 0.54,
                    color: '#fff',
                    padding: screenPadding, 
                }}>{'confirm_send:description'}</Text>

                <View style={{
                    backgroundColor: Colors.lightBackground,
                    padding: 30
                }}>
                    <Text style={styles.textStyle}>{'confirm_send:send_to'}</Text>
                    <Text style={styles.mutedText}>{params.address}</Text>
                    <Text style={styles.textStyle}>{'common:amount'}</Text>
                    <Text style={styles.mutedText}>{params.amount} {'BGL'}</Text>
                </View>
                <View style={{padding: screenPadding}}>
                    <PrimaryButton
                        loading={this.state.publishingTransaction} 
                        disabled={this.state.publishingTransaction}
                        onPress={this._handleSendTransaction}
                    >
                       {'common:actions.send'} 
                    </PrimaryButton>
                </View>
            </ScrollView>
        )
    }

}

export default SendConfimationScreen;


const styles = StyleSheet.create({

    container: {
        ...baseStyles.screen,
    },
    textStyle: {
        alignSelf: "stretch",
        color: "#eee",
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
    },
    mutedText: {
        paddingBottom: 10,
        color: "#7F8084"
    },

})