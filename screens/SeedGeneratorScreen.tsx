import React, { useState, useEffect } from 'react';
import { ScrollView, Image, View, StyleSheet, Share } from 'react-native';
import { WebBrowser } from 'expo';

import { Text } from '../components/Themed';
import { Caption, Bold } from '../components/StyledText';
import { PrimaryButton } from '../components/StyledButton';
import Radio, { Option } from '../components/Radio';

import baseStyles from '../styles';
import Colors from '../constants/Colors';

//const bip39 = require('bip39');

const useMnemonic = () => {
    const [mnemonic, setMnemonic] = useState();
    const [hasStored, setHasStored] = useState(false);
    const generateMnemonic = async () => {
        try {
            //setMnemonic(bip39.generateMnemonic())
            setMnemonic("weather fog theory chief coast eyebrow shuffle curve tennis inch toy reduce")
        } catch(e) {
            console.error(e)
        }
    }
    useEffect(() => {
        generateMnemonic()
    })

    const storedSuccessfully = () => {
        setHasStored(true);
    };

    return {
        mnemonic,
        hasStored,
        storedSuccessfully
    }
}

export const SeedGeneratorScreen = ({navigation}: any) => {
	const {mnemonic, hasStored, storedSuccessfully} = useMnemonic();

	const _openPaperWallet = () => {
		WebBrowser.openBrowserAsync('https://paper-wallet.me/?utm_source=emco&utm_campaign=mobile_app');
	}
	const _handleContinuePress = () => {
		navigation.navigate('ConfirmSeed', {seed: mnemonic});
	}
	const _handleSharePress = async () => {
        Share.share({ message: mnemonic });
    };
		
    return (
        <ScrollView
            style={styles.container}
        >
            <View style={styles.content}>
                <Caption>
                    create_wallet:description
                    {" "}
                    <Text 
                        // style={baseStyles.link}
                        onPress={_openPaperWallet}
                    >
                        Paper Wallet
                    </Text>
                </Caption>
                <Bold style={styles.cautionMessage}>
                    create_wallet:loose
                </Bold>
            </View>
            <View style={styles.inputContainer}>
                <Text style={styles.seedText}>
                    {mnemonic}
                </Text>
            </View>
            <View style={styles.content}>
                <Caption style={styles.backupCaption}>
                    <Text
                        onPress={_handleSharePress} 
                        // style={[baseStyles.link, {alignItems: 'center'}]}
                    >
                        create_wallet:copy
                    </Text>
                </Caption>
                <Radio
                    style={styles.radioCaption}
                    onSelect={storedSuccessfully}
                    defaultSelect={hasStored ? 1 : 0}
                >
                    <Option selectedColor={Colors.primary}>
                        <Caption>create_wallet:stored</Caption>
                    </Option>
                </Radio>
                <PrimaryButton disabled={!hasStored} onPress={_handleContinuePress}>
                    common:continue
                </PrimaryButton>
            </View>
        </ScrollView>
    )
};

export default SeedGeneratorScreen

const styles = StyleSheet.create({
	container: {
		...baseStyles.screen
	},
	imageContainer: {
		alignItems: 'center',
		marginBottom: 37
	},
	content: {
		padding: 30
	},
	cautionMessage: {
		marginTop: 10
	},
	inputContainer: {
		backgroundColor: 'rgba(255,255,255, 0.2)',
		paddingTop: 27,
		paddingLeft: 43,
		paddingRight: 43,
		paddingBottom: 43,
		alignItems: 'center'
	},
	seedText: {
		textAlign: 'center',
		color: '#fff',
		fontSize: 16,
		lineHeight: 24
	},
	backupCaption: {
		textAlign: 'center',
		marginBottom: 10
	},
	radioCaption: {
		marginBottom: 20
	}
});