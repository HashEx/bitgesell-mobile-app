import React from 'react';
import { View, StyleSheet, RefreshControl, Text, Alert, TouchableHighlight, FlatList } from 'react-native';

import * as walletSerice from '../services/walletService';
import Transaction from '../components/Transaction';

import baseStyles from "../styles";
import Colors from '../constants/Colors';
import Loader from '../components/Loader';
import { ITransaction } from '../interfaces/Transaction';

interface IProps {
    navigation: any
}

interface IState {
    transactions: ITransaction[],
    currentPage: number,
    isLoading: boolean,
    isLoadingMore: boolean,
    canLoadMore: boolean
}

export default class TransactionsScreen extends React.Component<IProps, IState> {

	constructor(props: IProps) {
		super(props);

		this.state = {
			transactions: [],
			currentPage: 1,
			isLoading: true,
			isLoadingMore: false,
			canLoadMore: true
		};
	}

	async componentDidMount() {
		await this._loadMoreTransactionsAsync();
		this.setState({isLoading: false})
	}

	_loadMoreTransactionsAsync = async () => {
        const canLoadMore = this.state.canLoadMore && !this.state.isLoadingMore;
        if(canLoadMore){
            this.setState({isLoadingMore: true})
            const page = this.state.currentPage;
            try {
                const txs = await walletSerice.getTransactions(page, 20);
                if(txs.length === 0) {
                    this.setState({canLoadMore: false})
                } else {
                    this._onDataArrived(txs);
                    this.setState({ currentPage: page + 1 })
                }
            } catch (ex) {
                console.log(ex);
                Alert.alert('could not load transactions. ' + ex)
            }
            this.setState({isLoadingMore: false})
        }
	}

	_onReload = async () => {
		this.setState({
			currentPage: 1,
			canLoadMore: true,
			transactions: [],
			isLoading: true,
		}, async () => {
            await this._loadMoreTransactionsAsync();
            this.setState({
                isLoading: false
            })
        })
		
	}

	_onDataArrived(newData: ITransaction[]) {
		const allTxs: ITransaction[] = [...this.state.transactions, ...newData];
		this.setState({
			transactions: allTxs,
		})
	}

	_handleTxPress = (tx: ITransaction) => () => {
        const { navigation } = this.props;
		navigation.navigate('Transaction', {tx});
	}

	_renderTx = ({item}: {item: ITransaction}) => {
        return (
            <View style={styles.txContainer}>
				<Transaction tx={item} onPress={this._handleTxPress(item)}/>
            </View>
        )
	}

	_renderRefreshControl = () => {
		return (
			<RefreshControl
				refreshing={false}
				onRefresh={this._onReload} />
		)
	}

	render(){
		const { isLoading, transactions } = this.state;
        if(isLoading) return <View style={styles.container}><Loader /></View>
		return (
			<View style={styles.container}>
                <FlatList 
                    data={transactions}
                    renderItem={this._renderTx}
                    refreshControl={this._renderRefreshControl()}
                    initialNumberToRender={20}
                    onEndReached={this._loadMoreTransactionsAsync}
                    onEndReachedThreshold={0.5}
                    style={{
                        backgroundColor: Colors.background
                    }}
                />
			</View>
		)
	}

}

const styles = StyleSheet.create({
	container: {
		...baseStyles.screen,
		backgroundColor: Colors.background,
		flex: 1
	},
	txContainer: {
		paddingLeft: 30,
		paddingRight: 30
	}
});