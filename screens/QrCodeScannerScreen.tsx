'use strict';

import React, { Component } from 'react';

import {
    StyleSheet,
    View,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import Colors from '../constants/Colors';

interface IProps {
    navigation: any;
    route: any;
}

const QrCodeScannerScreen: React.FC<IProps> = ({route, navigation}) => {

    const onSuccess = (e: any) => {
        route.params.returnData(e.data);
        navigation.goBack();
    };

    return (
        <QRCodeScanner
            style={styles.container}
            onRead={onSuccess}
            topViewStyle={styles.container}
            showMarker={true}
            markerStyle={styles.markerStyle}
            bottomViewStyle={styles.container}
            topContent={<View style={styles.container} />}
            bottomContent={<View style={styles.container} />}
        />
    );
}

export default QrCodeScannerScreen;

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.background
    },
    markerStyle: {
        borderColor: Colors.primary
    }
});