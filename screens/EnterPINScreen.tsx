import React from 'react';
import { ScrollView, StyleSheet, Alert } from 'react-native';

import Colors from '../constants/Colors';

import PIN from '../components/PIN';
import { PIN_STATUS } from '../types';

//import { unlockKey } from '../utils/keystorage';

interface IProps {
    route: any;
    navigation: any;
}


export class PINCodeScreen extends React.Component<IProps> {

	_onEnter = async (pin: string) => {
		const { params } = this.props.route;
		if(params.onConfirmed) {
			this.props.navigation.goBack(null);
			params.onConfirmed();
		} else {
            try {
			    // await unlockKey(pin)
			    this.props.navigation.navigate("Main");
			} catch(error) {
                console.log(error);
                Alert.alert('', 'pin:errors.not_valid');
            }
		}
	}

	render(){
		return (
			<ScrollView
				style={styles.container}
				contentContainerStyle={styles.contentContainer} >
				<PIN
					status={PIN_STATUS.ENTER}
					onEnter={this._onEnter}
				/>
			</ScrollView>
		)
	}
}

export default PINCodeScreen;

const styles = StyleSheet.create({
  container: {
    padding: 30,
    height: '100%',
    backgroundColor: Colors.background,
  },
  contentContainer: {
  	flex: 1,
  	justifyContent: 'center'
  }
});