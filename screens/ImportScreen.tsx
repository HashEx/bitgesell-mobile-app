import React, { useState } from 'react';
import { View, Text, StyleSheet } from 'react-native';

import { Header1Text, Caption } from '../components/StyledText';
import { PrimaryButton } from '../components/StyledButton';

import Screen from '../components/Screen';
import Input from '../components/Input';


export const ImportScreen = ({navigation}: any) => {
    const [seed, setSeed] = useState<string>("");
	const _onContinue = () => {
		navigation.navigate('SetPIN', {seed: seed.trim()});
	}
	const _onInput = (seed: string) => {
		seed = seed.toLowerCase().replace(/\s\s+/g, " ");
		setSeed(seed);
	}
		
    const canContinue = seed.split(" ").filter(item => item).length === 12;
    return (
        <Screen
            style={styles.container}
            contentContainerStyle={styles.contentContainer}
        >
            <View>
                <Header1Text>import_seed:title</Header1Text>
                <Caption>
                    import_seed:description
                </Caption>
            </View>
            <View>
                <Input
                    style={styles.input}
                    multiline={true}
                    numberOfLines={4}
                    onChangeText={_onInput}
                    value={seed}
                />	
            </View>
            <View>
                <PrimaryButton
                    disabled={!canContinue}
                    onPress={_onContinue}
                >
                    common:continue
                </PrimaryButton>
            </View>
        </Screen>
    )
};

export default ImportScreen;

const styles = StyleSheet.create({
	container: {
		paddingLeft: 30,
		paddingRight: 30
	},
	contentContainer: {
		flex: 1,
		justifyContent: 'center'
	},
	input: {
		paddingTop: 28,
		paddingBottom: 28,
		paddingLeft: 21,
		paddingRight: 21,
		textAlign: 'center',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 5,
		marginTop: 37,
		marginBottom: 22,
		fontSize: 16,
		lineHeight: 24,
		minHeight: 134,
		flex: 0
	}
})