import React from "react";
import {
    ScrollView,
    Text,
    Image,
    View,
    StyleSheet,
    Share,
    TouchableHighlight
} from "react-native";
// import RNQRCode from 'react-native-qrcode-svg';

import Colors from "../constants/Colors";
import { PrimaryButton } from "../components/StyledButton";
import * as walletService from '../services/walletService';

interface IProps {
    navigation: any;
}

export class ReceiveScreen extends React.Component<IProps> {
    walletAddress = walletService.getMyAddress();

    _shareAddress = async () => {
        Share.share({ message: this.walletAddress });
    };


    render() {
        return (
            <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                <View>
                    <Text style={styles.textStyle}>
                        {'receive:description'}
                    </Text>
                </View>
                <View style={styles.cardContainer}>
                    <View style={styles.cardHeaderContainer}>
                        <Text style={styles.lightTextStyle}>
                            {'receive:wallet_address'}
                        </Text>
                        <TouchableHighlight onPress={this._shareAddress}>
                            <Text>Share</Text>
                        </TouchableHighlight>
                    </View>

                    <Text>{this.walletAddress}</Text>

                    {/* <View style={styles.qrCodeContainer}>
                        <RNQRCode value={this.walletAddress} />
                    </View> */}
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: Colors.background,
        padding: 30
    },
    redeemContainer: {
        paddingVertical: 30
    },
    cardHeaderContainer: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: 'flex-end',
        marginBottom: 10
    },
    textStyle: {
        fontSize: 16,
        opacity: 0.54,
        color: "#fff"
    },
    lightTextStyle: {
        fontSize: 16,
        opacity: 0.54
    },
    cardContainer: {
        marginTop: 40,
        marginBottom: 10,
        padding: 20,
        backgroundColor: "#fff",
        borderRadius: 5
    },
    qrCodeContainer: {
        padding: 20,
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    }
});

export default ReceiveScreen;
