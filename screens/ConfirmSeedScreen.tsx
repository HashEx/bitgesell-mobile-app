import React from 'react';
import { ScrollView, StyleSheet } from 'react-native';

import baseStyles from '../styles';

import { Caption } from '../components/StyledText';
import SeedConstructor from '../components/SeedConstructor';


export const ConfirmSeedScreen = ({route, navigation}: any) => {
    const { seed } = route.params;
	const _onContinue = () => {
		navigation.navigate('SetPIN', {seed: seed});
	}
    return (
        <ScrollView 
            style={styles.container}
            contentContainerStyle={styles.contentContainer}
        >
            <Caption style={styles.caption}>
                confirm_seed:description
            </Caption>
            {seed && (
                <SeedConstructor
                    seed={seed}
                    onContinue={_onContinue}
                />
            )}
        </ScrollView>
    )
}

export default ConfirmSeedScreen;

const styles = StyleSheet.create({
	container: {
		paddingLeft: 30,
		paddingRight: 30,
		...baseStyles.screen
	},
	contentContainer: {
		flex: 1,
		justifyContent: 'space-around'
	},
	caption: {
		fontSize: 12
	}
});