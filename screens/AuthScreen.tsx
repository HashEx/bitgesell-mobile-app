import React from 'react';
import { ScrollView, View, Image, Text, AsyncStorage, StyleSheet } from 'react-native';

import { PrimaryButton, SecondaryButton } from '../components/StyledButton';

import Colors from '../constants/Colors';


export const SignInScreen = ({navigation}: any) => {

    const _createWalletAsync = () => {
        navigation.navigate('SeedGenerator');
    };

    const _connect =  () => {
        navigation.navigate('ImportSeed');
    }

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
      >
        <View>
          <Text style={styles.title}>Bitgesell Mobile App</Text>
        </View>
        <View>
          <Text style={styles.subTitle}>Crypto wallet kickstart (powered by Bitgesell)</Text>  
        </View>
        <View style={styles.buttonsContainer}>
          <View>
            <View style={styles.buttonPrimary}>
              <PrimaryButton
                onPress={_createWalletAsync}
              >
                Create
              </PrimaryButton>
            </View>
            <SecondaryButton onPress={_connect}>
                Restore
            </SecondaryButton>    
          </View>
        </View>
      </ScrollView>
    );
}

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.background
  },
  contentContainer: {
    paddingVertical: 30,
    flex: 1,
    justifyContent: 'space-around'
  },
  image: {
    width: '100%'
  },
  textContainer: {
    paddingLeft: 30,
    paddingRight: 30
  },
  title: {
    fontSize: 25,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
    marginBottom: 13
  },
  subTitle: {
    fontSize: 16,
    textAlign: 'center',
    color: 'rgba(255,255,255,0.6)'
  },
  buttonsContainer: {
    marginTop: -20,
    paddingLeft: 30,
    paddingRight: 30
  },
  buttonPrimary: {
    marginBottom: 20
  }
})