import React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';

import Colors from '../constants/Colors';

import PIN from '../components/PIN';

interface IProps {
    navigation: any
    route: any
}

export default class PINCodeScreen extends React.Component<IProps> {
	_onConfirm = async (pin: string) => {
		const { seed } = this.props.route.params;
		this.props.navigation.navigate('WalletSuccess', {pin: pin, seed: seed});
	}
	render(){
		return (
			<ScrollView
				style={styles.container}
				contentContainerStyle={styles.contentContainer}
			>
				<PIN
					status='choose'
					onConfirm={this._onConfirm}
				/>
			</ScrollView>
		)
	}
}

const styles = StyleSheet.create({
  container: {
    padding: 30,
    height: '100%',
    backgroundColor: Colors.background,
  },
  contentContainer: {
  	flex: 1,
  	justifyContent: 'center'
  }
});