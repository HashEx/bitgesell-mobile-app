import React from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  RefreshControl,
  TouchableHighlight,
  Alert
} from 'react-native';

import { Feather } from '@expo/vector-icons';
import Modal from "react-native-modal";

import * as walletService from '../services/walletService';

import { Caption, Header2Text, Header3Text } from "../components/StyledText";

import Colors from '../constants/Colors';

import { PrimaryButton, SecondaryButton } from '../components/StyledButton';
import Link from '../components/Link';
import Wallet from '../components/Wallet';
import TransactionsList from '../components/TransactionsList';
import { ITransaction } from '../interfaces/Transaction';


interface IProps {
  navigation: any
}

interface IState {
  txs: ITransaction[];
  balance: number;
  fetchingTxs: boolean;
  fetchingBalance: boolean;
  refreshing: boolean;
}

export class WalletScreen extends React.Component<IProps, IState> {
  _interval: number | null = null;
  constructor(props: IProps){
    super(props);
    this.state = {
      txs: [],
      fetchingTxs: false,
      fetchingBalance: false,
      refreshing: false,
      balance: 0.000,
    } as IState
  }
  componentDidMount() {
    this._fetchData();
    this._interval = setInterval(() => {
      console.log('updating balances');
      this._updateBalance();
    }, 30000);
  }
  componentWillUnmount() {
    clearInterval(this._interval);
  }
  _getTransactions = async () => {
    this.setState({fetchingTxs: true});
    try {
      const txs = await walletService.getTransactions(1, 10)
      this.setState({
        txs,
        fetchingTxs: false
      });
    } catch(err)  {
      this.setState({fetchingTxs: false});
      Alert.alert('','could not fetch transactions: ' + err);
      console.log(err);
    }
  }
  _getBalance = async () => {
    this.setState({fetchingBalance: true});
    try{
      const balance = await walletService.getBalance();
      this.setState({
        balance,
        fetchingBalance: false,
      });
    } catch (e) {
      console.log(e)
      this.setState({ fetchingBalance: false });
    }
  }

  _updateBalance = async () => {
    try {
      const balance = await walletService.getBalance();

      //update transactions list if balances changed
      if(balance !== this.state.balance) {
        
        this.setState({balance});
        this._getTransactions();
        
      }
    } catch(ex) {
      console.log('could not update balances', ex)
    }
  }

  _navigateToSend = () => {
    this.props.navigation.navigate('Send')
  }
  _navigateToReceive = () => {
    this.props.navigation.navigate('Receive')
  }
  _handleViewAllPress = () => {
    this.props.navigation.navigate('Transactions'); 
  }
  _handleTxPress = (tx: Transaction) => () => {
    this.props.navigation.navigate('Transaction', {tx});
  }
  _fetchData = () => {
    this._getTransactions();
    this._getBalance();
  }
  _onRefresh = () => {
    this._fetchData();
  }
  render() {
    const {
      fetchingTxs,
      fetchingBalance,
      balance,
      txs,

    } = this.state;
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }
      >

        <Wallet
          title="BGL"
          value={balance}
          loading={fetchingBalance}
        />
        <View style={styles.content}>
          <View style={styles.buttonsContainer}>
            <View style={styles.buttonContainer}>
              <PrimaryButton style={styles.button} onPress={this._navigateToSend}>
                {'common:actions.send'} <Feather name="arrow-right" size={16} />
              </PrimaryButton>  
            </View>
            <View style={styles.buttonContainer}>
              <SecondaryButton style={styles.button} onPress={this._navigateToReceive}>
                {'common:actions.receive'} <Feather name="arrow-left" size={16} />
              </SecondaryButton>  
            </View>
          </View>
        </View>
        <View style={styles.content}>
          <View style={styles.transactionsHeader}>
            <Header2Text>{'wallet:transactions'}</Header2Text>
            <Link onPress={this._handleViewAllPress}>{'common:actions.view_all'}</Link>
          </View>
          <TransactionsList
            loading={fetchingTxs}
            transactions={txs.slice(0, 5) }
            onTxPress={this._handleTxPress}
          />
        </View>
      </ScrollView>
    );
  }
}

export default WalletScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
    paddingTop: 20
  },
  contentContainer: {
    paddingBottom: 50
  },
  content: {
    paddingLeft: 30,
    paddingRight: 30,
    paddingBottom: 10
  },
  transactionsHeader: {
    flex: 1,
    justifyContent: 'space-between',
    flexDirection: 'row'
  },
  buttonsContainer: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: -5,
    marginRight: -5,
    marginBottom: 5
  },
  buttonContainer: {
    width: '50%',
    paddingLeft: 5,
    paddingRight: 5
  },
  voucherQuestion: {
    color: '#fff',
    paddingTop: 20,
    opacity: 0.54,
    paddingBottom: 20,
    textAlign: "center"
  },
    modalContent: {
        backgroundColor: Colors.background,
        padding: 22,
        // justifyContent: "center",
        // alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
      },
    modalHeader: {
        marginBottom: 0
    }, 
    modalText: {
        marginBottom: 10
    }

});
