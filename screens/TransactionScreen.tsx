import React from 'react';
import { View, StyleSheet, Text, Linking, ViewProps } from 'react-native';
import { Feather } from '@expo/vector-icons';

import { TX_DIRECTION } from '../types';

import Colors from '../constants/Colors';

import Screen from '../components/Screen';
import InfoItem from '../components/InfoItem';
import { ITransaction } from '../interfaces/Transaction';


const TransactionIcon: React.FC<ViewProps & {direction: TX_DIRECTION}> = ({direction, style}) => {
	if(direction === TX_DIRECTION.IN){
		return (
			<View style={style}>
				<Feather
					name="arrow-right"
					color={Colors.primary}
					size={36}
				/>
			</View>
		)
	}else{
		return (
			<View style={style}>
				<Feather
					name="arrow-left"
					color="#fff"
					size={36}
				/>
			</View>
		)
	}
}

const MainInfo = ({ tx }: { tx: ITransaction }) => {
	const isTxIn = tx.amount > 0;
	const direction = isTxIn ? TX_DIRECTION.IN : TX_DIRECTION.OUT;
	return (
		<View style={styles.mainInfoContainer}>
			<View style={[styles.mainInfo, isTxIn ? styles.mainInfoOut : null]}>
				<TransactionIcon
					style={styles.mainInfoIcon}
					direction={direction}
				/>
				<Text style={[styles.mainInfoStatus]}>{'transaction:status.' + tx.status}</Text>
			</View>
		</View>
	)
}

interface ITransactionScreenProps {
  route: {
    params: {
      tx: ITransaction
    }
  }
}

export const TransactionScreen: React.FC<ITransactionScreenProps> = ({route}) => {
  const { tx } = route.params;
  return (
    <Screen style={styles.container}>
      <MainInfo tx={tx} />
      <InfoItem label={'common:amount'}>
        {tx.amount} {tx.currency}
      </InfoItem>
      <InfoItem label={'transaction:from'}>
        {tx.from}
      </InfoItem>
      <InfoItem label={'transaction:to'}>
        {tx.to}
      </InfoItem>
      <InfoItem label={'transaction:date'}>
        {tx.date}
      </InfoItem>
      <InfoItem label={'transaction:fee'}>
        {tx.fee}
      </InfoItem>
      <InfoItem label={'transaction:confirms'}>
        {tx.confirms}
      </InfoItem>
      <InfoItem label={'transaction:block_height'}>
        {tx.block}
      </InfoItem>
      <InfoItem label={'transaction:tx_hash'}>
        {tx.hash}
      </InfoItem>
      <InfoItem label="">
      </InfoItem>
      <InfoItem label="">
      </InfoItem>
    </Screen>
  )
}

export default TransactionScreen;

const styles = StyleSheet.create({
	container: {
		padding: 30
	},
	mainInfoContainer: {
		alignItems: 'center',
		marginBottom: 20
	},
	mainInfo: {
		height: 155,
		width: 155,
		borderRadius: 80,
		backgroundColor: 'rgba(149, 154, 166, 0.5)',
		borderWidth: 0.6,
		borderColor: 'rgb(149, 154, 166)',
		alignItems: 'center',
		justifyContent: 'center'
	},
	mainInfoOut: {
		backgroundColor: 'rgba(179, 125, 248, 0.1)',
		borderColor: 'rgb(179, 125, 248)'
	},
	mainInfoIcon: {},
	mainInfoStatus: {
		fontSize: 18,
		lineHeight: 35,
		color: 'rgba(255,255,255,0.7)'
	},
	mainInfoAmount: {
		fontSize: 16,
		lineHeight: 35,
		fontWeight:'bold',
		color: '#fff'
	}
});