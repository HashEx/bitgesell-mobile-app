import React from "react";
import { Ionicons } from '@expo/vector-icons';
import {
    ScrollView,
    View,
    Text,
    StyleSheet,
    TouchableHighlight,
    TouchableOpacity,
    Alert,
    Switch
} from "react-native";
import Modal from "react-native-modal";

import * as walletService from '../services/walletService';

import isAddress from '../utils/isAddress';

import { PrimaryButton } from "../components/StyledButton";

import { InputGroup } from "../components/Input";
import Link from "../components/Link";
import CoinLabel from "../components/CoinLabel";


import baseStyles from "../styles";


import { Caption, Header1Text, Header3Text } from "../components/StyledText";
import Colors from "../constants/Colors";


import { BorderlessButton } from '../components/StyledButton';

interface IMaxCaptionProps {
    amount: number;
    onInfoPress: () => void;
}

const MaxCaption: React.FC<IMaxCaptionProps> = ({ amount, }) => {
    return (
        <View style={styles.captionContainer}>
            <Caption style={{ paddingBottom: 0 }}>
                {'send:max' + ' ' + amount}
            </Caption>
        </View>
    )
}

interface IProps {
    navigation: any;
}

interface IState {
    address: string;
    amount: number;
    maxAmount: number;
    isBalancesInfoModalVisible: boolean;
}

export class SendScreen extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = {
            address: '',
            amount: 0,
            maxAmount: 0,
            isBalancesInfoModalVisible: false,
        }
    }

    componentDidMount() {
        this._fetchBalance();
    }

    _fetchBalance = async () => {
        const balance = await walletService.getBalance();
        this.setState({ maxAmount: balance });
    }
    _handleContinuePress = () => {
        const {  amount, address } = this.state;
        if (!isAddress(address)) {
            Alert.alert('', 'send:modals.error.eth_addr');
            return;
        }

        if (isNaN(amount)) {
            Alert.alert('', `send:modals.error.amount ${amount}`);
            return;
        }

        if (amount <= 0) {
            Alert.alert('', 'send:modals.error.subzero');
            return;
        }

        this.props.navigation.navigate('ConfirmTransaction', {
            address: address,
            amount: amount.toString(),
        })
    }

    _returnData = (address: string) => {
        this.setState({ address: address })
    }

    _toggleBalancesInfoModal = () => {

    }

    _handleAddressChange = (text: string) => {
        this.setState({ address: text })
    }
    
    _handleAmountChange = (value: string) => {
        this.setState({ 
            amount: parseFloat(value.replace(',', '.')) 
        })
    }


    render() {
        const { address, amount, maxAmount } = this.state;
        const isAmountInvalid = amount ? maxAmount > 0 && amount > maxAmount : true;
        const maxCaption = maxAmount > 0 ? <MaxCaption amount={maxAmount} onInfoPress={this._toggleBalancesInfoModal} /> : null;
        const currencySymbol = "BGL";
        const disabled: boolean = !isAddress(address) || isAmountInvalid; 
        return (
            <ScrollView style={styles.container}>
                <View style={styles.content}>
                    <InputGroup
                        label={<Text>{'send:send_to'}</Text>}
                        inputStyle={styles.textInputStyle}
                        placeholder={'send:enter_address'}
                        onChangeText={this._handleAddressChange}
                        isInvalid={address && !isAddress(address)}
                        value={address}
                    />
                    <InputGroup
                        suffix={currencySymbol}
                        label={<Text>{'common:actions.send'}</Text>}
                        inputStyle={styles.textInputStyle}
                        isInvalid={isAmountInvalid}
                        keyboardType='numeric'
                        placeholder={'send:enter_amount'}
                        onChangeText={this._handleAmountChange}
                    />
                    <View><Text>{amount}</Text></View>
                    {maxCaption}
                </View>
                <View style={styles.content}>
                    <PrimaryButton onPress={this._handleContinuePress} disabled={disabled}>
                        {'common:continue'}
                    </PrimaryButton>
                </View>
            </ScrollView>
        );
    }
}

export default SendScreen;

const screenPadding = 30;

const styles = StyleSheet.create({
    container: {
        ...baseStyles.screen
    },
    content: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: screenPadding,
        paddingRight: screenPadding
    },
    contentContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "space-around"
    },
    textStyle: {
        alignSelf: 'flex-start',
        color: "#eee",
        fontSize: 16,
        paddingTop: 10,
        paddingBottom: 10,
    },
    link: {
        fontWeight: "bold",
        paddingTop: 5,
        paddingBottom: 5,
        textAlign: "right",
        alignSelf: 'flex-end',
    },
    bordered: {
        borderColor: "#aaa",
        borderRadius: 6,
        borderWidth: 2,
        padding: 2,
        backgroundColor: "#36373C",
        borderColor: "#aaa"
    },
    textInputStyle: {
        padding: 4,
        color: "#fff",
        backgroundColor: "#36373C"
    },
    mutedText: {
        paddingTop: 10,
        paddingBottom: 10,
        paddingHorizontal: screenPadding,
        color: "#7F8084"
    },
    currencyStyle: {
        padding: 5,
        color: '#fff',
        fontSize: 16
    },
    captionContainer: {
        flex: 1,
        flexDirection: 'row',
        alignSelf: 'flex-end',
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoIcon: {
        color: 'rgba(255,255,255,0.54)',
        fontSize: 28,
        marginStart: 10
    },
    modalContent: {
        backgroundColor: Colors.background,
        padding: 22,
        // justifyContent: "center",
        // alignItems: "center",
        borderRadius: 4,
        borderColor: "rgba(0, 0, 0, 0.1)"
    },
    modalHeader: {
        marginBottom: 0
    },
    modalText: {
        marginBottom: 10
    }
});
